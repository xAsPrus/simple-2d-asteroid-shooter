using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Gameplay
{
    public class InputController : InitializableMonobehaviour<GameManager>
    {
        [SerializeField] List<KeyAction> _KeyBindings;
        

        private void Update()
        {
            if(IsInitialized)
            {
                if(manager.IsGamePlaying)
                {
                    for (int i = 0; i < _KeyBindings.Count; i++)
                    {
                        _KeyBindings[i].Detect();
                    }
                }

            }
        }


    }
    [System.Serializable]
    public class KeyAction
    {//TODO: add detecting combination of keys, but not needed for this project at this moment (26.06.2021)
        public string Name;
        public List<KeyCode> Key;
        [SerializeField] EventActions Events;
        [System.Serializable]
        public class EventActions
        {
            public UnityEventAction OnDown;
            public UnityEventAction OnHolding;
            public UnityEventAction OnUp;
        }

        public void Detect()
        {//TODO: make that whole detection better - detecting Unity's Input more automaticaly - Dictionary with events?
            bool[] LaunchedMethods = new bool[3];//this exists to prevent multi lauching same method. Basicly thats for "OR" operation between list "Key"
            for (int i = 0; i < Key.Count; i++)
            {
                if (!LaunchedMethods[0] && Events.OnDown != null && Input.GetKeyDown(Key[i]))
                {
                    Events.OnDown.Invoke();
                    LaunchedMethods[0] = true;
                }
                if (!LaunchedMethods[1] && Events.OnHolding!= null && Input.GetKey(Key[i]))
                {
                    Events.OnHolding.Invoke();
                    LaunchedMethods[1] = true;
                }
                if (!LaunchedMethods[2] && Events.OnUp!= null && Input.GetKeyUp(Key[i]))
                {
                    Events.OnUp.Invoke();
                    LaunchedMethods[2] = true;
                }
            }
        }
    }

    [System.Serializable]
    public class UnityEventAction: UnityEvent
    {
    }

}