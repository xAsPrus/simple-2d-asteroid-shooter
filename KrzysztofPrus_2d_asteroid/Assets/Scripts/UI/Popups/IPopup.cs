using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPopup
{
    public abstract void Show();
    public abstract void Hide();

}
