using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class GameFinishedPopup : InitializableMonobehaviour<UiController>, IPopup
    {
        public void BtnRestart()
        {
            manager.GetManager.RestartGame();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }
    }

}
