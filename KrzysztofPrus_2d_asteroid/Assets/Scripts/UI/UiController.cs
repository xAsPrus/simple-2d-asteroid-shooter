using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Gameplay
{
    public class UiController : InitializableMonobehaviour<GameManager>
    {
        [SerializeField] Text _Score;
        [SerializeField] Text _FPS;
        [SerializeField] List<InitializableMonobehaviour<UiController>> _Popups;

        public GameManager GetManager => manager;


        public T GetPopup<T>() where T: InitializableMonobehaviour<UiController>, IPopup
        {
            return _Popups.Find((x) => x is T) as T;
        }

        public override void Initialize(GameManager gameManager)
        {
            if(!IsInitialized)
            {
                _Popups.ForEach((x) => x.Initialize(this));
                base.Initialize(gameManager);
            }
        }
        protected override void OnInitialize()
        {
            manager.ScoreManager.OnScoreChanged += UpdateScore;
        }

        void UpdateScore(int oldValue, int newValue)
        {
            _Score.text = newValue.ToString();
        }

        private void Update()
        {
            if(IsInitialized)
            {
                _FPS.text = (Time.frameCount / Time.time).ToString();
            }
        }


    }
}
