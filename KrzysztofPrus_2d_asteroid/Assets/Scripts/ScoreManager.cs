namespace Gameplay
{
    public class ScoreManager : InitializableObject<GameManager>
    {
        public delegate void IntValueOnChange(int oldValue, int NewValue);
        public event IntValueOnChange OnScoreChanged;

        public int Score { get; private set; } = 0;

        public void Initialize(GameManager manager, ref System.Action<int> scoreSetter)
        {
            if (!IsInitialized)
            {
                base.Initialize(manager);
                scoreSetter += SetScore;

            }
        }


        private void SetScore(int newValue)
        {
            if (newValue != Score)
            {
                int oldScore = Score;
                Score = newValue;

                OnScoreChanged?.Invoke(oldScore, Score);
            }
        }


    }

}
