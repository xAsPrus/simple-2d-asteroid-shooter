﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PoolSystem : MonoBehaviour
{
    static PoolSystem Instance { get; set; }

    [SerializeField] List<Poolable> _Prefabs;

    #region Private Vars

    Dictionary<System.Type, List<Poolable>> pooledObjects;
    private bool IsInitialized = false;

    #endregion

    #region Public API

    public static List<T> GetAllCreatedPoolable<T>() where T : Poolable
    {
        if (Instance.pooledObjects.ContainsKey(typeof(T)))
            return Instance.pooledObjects[typeof(T)].Cast<T>().ToList();
        return new List<T>();
    }

    public static T GetFreePoolable<T>() where T:Poolable
    {
        bool InstatiatedNew;
        return Instance.getFreePoolable<T>(out InstatiatedNew);
    }
    public static T GetFreePoolable<T>(out bool InstatiatedNew) where T:Poolable
    {
        return Instance.getFreePoolable<T>(out InstatiatedNew);
    }
    #endregion

    #region Private Methods
    private void Awake()
    {
        Instance = this;
    }
    private void Initialize()
    {
        pooledObjects = new Dictionary<System.Type, List<Poolable>>();
        IsInitialized = true;
    }

    private T getFreePoolable<T>(out bool InstatiatedNew) where T :Poolable
    {
        if (!IsInitialized)
            Initialize();
        InstatiatedNew = false;
        System.Type SelectedType = typeof(T);

        if(!pooledObjects.ContainsKey(SelectedType))
            pooledObjects.Add(SelectedType, new List<Poolable>());

        T FoundFreeObject = pooledObjects[SelectedType].Find((x) => x.IsFree) as T;
        if (FoundFreeObject == null)
            pooledObjects[SelectedType].Add(FoundFreeObject = GetNewInstance(out InstatiatedNew));


        return FoundFreeObject;

        T GetNewInstance(out bool InstatiatedNew)
        {
            Poolable objPrefab = _Prefabs.Find(x => x is T);
            T newObj = Instantiate(objPrefab.gameObject).GetComponent<T>();
            newObj.Deactivate();
            InstatiatedNew = true;
            return newObj;
        }
    }

    #endregion

}
