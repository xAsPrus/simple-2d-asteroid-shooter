﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poolable : MonoBehaviour
{
    public bool IsFree { get; private set; }

    public virtual void Activate()
    {
        gameObject.SetActive(true);
        IsFree = false;
    }

    [ContextMenu("Deactivate")]
    public virtual void Deactivate()
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
        IsFree = true;
    }




}
