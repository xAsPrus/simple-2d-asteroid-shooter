using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class MapManager : InitializableMonobehaviour<GameManager>
    {
        static readonly System.TimeSpan TIME_TO_RESPAWN_ASTEROID = new System.TimeSpan(0, 0, 1);
        static readonly Vector2 MAP_START_SPAWN_POSITION_OFFSET = new Vector2(.5f, .5f);
        const int CREATING_OBJECTS_PER_FRAME = 300;
        const int MAX_RESPAWNING_ASTEROIDS_PER_FRAME = 20;
        [SerializeField] Vector2Int _MapSize;
        [SerializeField] Vector2Int _AsteroidRespawnMapSize;
        [SerializeField] Transform _MapParent;

        private List<System.DateTime> DestroyedAsteroidTimes;
        private bool FrameSplitEnable = false;
        private bool IsInitializationStarted = false;
        public void Initialize(GameManager gameManager, bool frameSplitEnable)
        {
            if (!IsInitialized && !IsInitializationStarted)
            {
                AsteroidsForcesEnabled = true;
                IsInitializationStarted = true;
                FrameSplitEnable = frameSplitEnable;
                manager = gameManager;
                DestroyedAsteroidTimes?.Clear();
                DestroyedAsteroidTimes = new List<System.DateTime>();
                StartCoroutine(MapCreating());
                StartCoroutine(RespawningAsteroids());
                Asteroid.OnDestroyAny += () => { if (this != null) onAsteroidDestroy(); };//protection from reloading scene
                OnInitialize();
            }
        }
        public static bool AsteroidsForcesEnabled { get; private set; } = true;
        public void StopAllAsteroidForces()
        {
            List<Asteroid> asteroids = PoolSystem.GetAllCreatedPoolable<Asteroid>();
            var asteroids2 = PoolSystem.GetAllCreatedPoolable<Asteroid>();
            AsteroidsForcesEnabled = false;
            asteroids.ForEach(x => x.DisableForces());
        }

        protected override void OnInitialize()
        {
            
        }

        IEnumerator RespawningAsteroids()
        {
            int RespawnedAsteroids = 0;
            while (true)
            {
                while (DestroyedAsteroidTimes?.Count > 0 && DestroyedAsteroidTimes[0] <= System.DateTime.Now)
                {
                    if (RespawnedAsteroids >= MAX_RESPAWNING_ASTEROIDS_PER_FRAME)
                    {
                        yield return null;
                        RespawnedAsteroids = 0;
                    }
                    CreateAsteroidAtRandomPosition();
                    RespawnedAsteroids++;
                    DestroyedAsteroidTimes.RemoveAt(0);
                }
                yield return null;
                RespawnedAsteroids = 0;
            }
        }
        Vector3 GetCameraWorldSize()
        {
            return new Vector3(manager.MainCamera.orthographicSize * ((float)manager.MainCamera.pixelRect.width / manager.MainCamera.pixelRect.height) * 2f, manager.MainCamera.orthographicSize * 2, 0);
        }

        Vector3 GetRandomPositionInPlayArea()
        {
            Bounds CamBounds = new Bounds(manager.MainCamera.transform.position, GetCameraWorldSize());
            Bounds WorldBounds = new Bounds(manager.MainCamera.transform.position, new Vector3(_AsteroidRespawnMapSize.x, _AsteroidRespawnMapSize.y, 0));
            Vector3 pos;

            do
            {
                pos = new Vector3(
                    Random.Range(WorldBounds.min.x, WorldBounds.max.x),
                    Random.Range(WorldBounds.min.y, WorldBounds.max.y), 0);
            } while (
            pos.x > CamBounds.min.x && pos.x < CamBounds.max.x &&
            pos.y > CamBounds.min.y && pos.y < CamBounds.max.y);

            return pos;
        }


        private void CreateAsteroidAtRandomPosition()
        {
            if (manager.MainCamera.orthographic)//case for
            {
                Asteroid asteroid = PoolSystem.GetFreePoolable<Asteroid>();
                asteroid.transform.position = GetRandomPositionInPlayArea();
                asteroid.transform.parent = _MapParent;
                asteroid.Activate();
            }
        }

        IEnumerator MapCreating()
        {
            int CurrentFrameObjectsCreated = 0;
            int CreataingPerFrame;
            if (FrameSplitEnable)
                CreataingPerFrame = CREATING_OBJECTS_PER_FRAME;
            else
                CreataingPerFrame = int.MaxValue;

            for (int y = 0; y < _MapSize.y; y++)
            {
                for (int x = 0; x < _MapSize.x; x++)
                {
                    CreateObject(new Vector2(x - _MapSize.x / 2, y - _MapSize.y / 2) + MAP_START_SPAWN_POSITION_OFFSET, out bool createdNew);
                    if (CurrentFrameObjectsCreated > CreataingPerFrame)
                    {
                        CurrentFrameObjectsCreated = 0;
                        yield return null;
                    }
                    CurrentFrameObjectsCreated++;
                }
            }
            IsInitialized = true;

            void CreateObject(Vector3 position, out bool crea)
            {
                Asteroid asteroid = PoolSystem.GetFreePoolable<Asteroid>(out crea);
                asteroid.transform.parent = _MapParent;
                asteroid.transform.position = position;
                asteroid.Activate();
            }

        }

        private void onAsteroidDestroy()
        {
            DestroyedAsteroidTimes.Add(System.DateTime.Now.Add(TIME_TO_RESPAWN_ASTEROID));
        }
    }

}
