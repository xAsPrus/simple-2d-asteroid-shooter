using System.Collections;
using UnityEngine;

namespace Gameplay
{
    public class PlayerController : InitializableMonobehaviour<GameManager>
    {
        #region Contsts
        const float SHOOTING_FREQUENCY = .5F;
        const float BULLET_LIFETIME = 3;
        const float BULLET_POWER = 6;

        #endregion

        public event System.Action OnAsteroidDestroy;
        public event System.Action OnPlayerDestroy;

        [Tooltip("How fast is player moving forward")] [SerializeField] float _ForwardSpeed;
        [Tooltip("How fast is player moving forward")] [SerializeField] float _ForwardMinimumSpeed;
        [Tooltip("How fast is player steering left or right")] [SerializeField] float _SteeringSpeed;
        [SerializeField] Transform _BulletSpawnPlace;
        [SerializeField] Bullet _BulletPrefab;

        #region Steering Methods 
        public void StartGoingForward()//method attached to InputController
        {
            CurrentDirection |= GameManager.Direction.Forward;
            RefreshForwardVector();
        }
        public void StopGoingForward()//method attached to InputController
        {
            CurrentDirection &= ~GameManager.Direction.Forward;
            RefreshForwardVector();
        }
        public void StartGoingLeft()//method attached to InputController
        {
            CurrentDirection |= GameManager.Direction.Left;
            RefreshRotateVector();
        }
        public void StopGoingLeft()//method attached to InputController
        {
            CurrentDirection &= ~GameManager.Direction.Left;
            RefreshRotateVector();
        }
        public void StartGoingRight()//method attached to InputController
        {
            CurrentDirection |= GameManager.Direction.Right;
            RefreshRotateVector();
        }
        public void StopGoingRight()//method attached to InputController
        {
            CurrentDirection &= ~GameManager.Direction.Right;
            RefreshRotateVector();
        }
        #endregion

        private Coroutine ShootingCoroutine;
        Vector2 PlayerForwardVector;
        Vector3 PlayerRotateVector;

        protected override void OnInitialize()
        {
            CurrentDirection = GameManager.Direction.none;
            ShootingCoroutine = StartCoroutine(ShootingRapidly(SHOOTING_FREQUENCY, BULLET_LIFETIME));
            RefreshForwardVector();
            RefreshRotateVector();
            OnAsteroidDestroy = null;
        }

        private void RefreshForwardVector()
        {
            PlayerForwardVector = new Vector2(0, (CurrentDirection.HasFlag(GameManager.Direction.Forward) ? _ForwardSpeed : _ForwardMinimumSpeed));
        }
        private void RefreshRotateVector()
        {
            PlayerRotateVector = new Vector3(0, 0, ((int)CurrentDirection%10)*_SteeringSpeed);
        }

        
        public GameManager.Direction CurrentDirection { get; private set; }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.TryGetComponent(out Asteroid asteroid))
            {
                OnPlayerDestroy?.Invoke();
            }
        }


        private void Update()
        {
            if (IsInitialized && manager.IsGamePlaying)
            {
                transform.Translate(PlayerForwardVector * Time.deltaTime, Space.Self);

                transform.Rotate(PlayerRotateVector * Time.deltaTime*360, Space.Self);
            }
        }

        #region BulletShooting
        IEnumerator ShootingRapidly(float RapidRate, float BulletLifetime)
        {
            float CurrentTime = 0;
            while (true)
            {
                yield return null;
                if (manager.IsGamePlaying)
                {
                    CurrentTime += Time.deltaTime;
                    if (CurrentTime >= RapidRate)
                    {
                        CurrentTime -= RapidRate;
                        Shoot(BulletLifetime);
                    }
                }
            }
        }

        void Shoot(float BulletLifetime)
        {
            Bullet b = PoolSystem.GetFreePoolable<Bullet>();
            b.transform.position = _BulletSpawnPlace.position;
            b.Launch(BULLET_POWER, BulletLifetime, transform.rotation);
            b.OnDie += OnBulletDie;
        }

        void OnBulletDie(Bullet.DeactivationReasonType reason)
        {
            if (reason == Bullet.DeactivationReasonType.Collide)
                OnAsteroidDestroy?.Invoke();
        }

        #endregion
    }

}
