using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{

    public class Bullet : Poolable
    {
        public enum DeactivationReasonType { LifeTime=0, Collide=1}

        public event System.Action<DeactivationReasonType> OnDie;

        Vector3 DirectionVector=Vector3.zero;
        public void Launch(float power, float Lifetime, Quaternion rotation)
        {
            transform.rotation = rotation;
            DirectionVector = new Vector3(0, power, 0);
            Activate();
            StartCoroutine(DelayedEvent(()=>Kill(DeactivationReasonType.LifeTime), Lifetime));
        }
        private void Update()
        {
            if(!IsFree)
                transform.Translate(DirectionVector * Time.deltaTime, Space.Self);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.gameObject.TryGetComponent(out Asteroid asteroid))
            {
                asteroid.Deactivate();
                Kill(DeactivationReasonType.Collide);
            }
        }
        public override void Deactivate()
        {
            OnDie = null;
            base.Deactivate();
        }
        private void Kill(DeactivationReasonType Reason)
        {
            OnDie?.Invoke(Reason);
            Deactivate();
        }

        IEnumerator DelayedEvent(System.Action onComplete, float time)
        {
            yield return new WaitForSeconds(time);
            onComplete?.Invoke();
        }
    }
}

