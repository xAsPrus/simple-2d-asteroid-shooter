using UnityEngine;

namespace Gameplay
{
    public sealed class GameManager : InitializableMonobehaviour<GameManager>
    {
        const int SCORE_FROM_DESTROYING_ASTEROID = 1;

        #region Types
        [System.Flags]
        public enum Direction { Right = -1, none =0, Left = 1, Forward=10 }

        #endregion

        #region Serialize Fields
        [SerializeField] UiController _UIController;
        [SerializeField] MapManager _MapManager;
        [SerializeField] PlayerController _PlayerController;
        [SerializeField] InputController _InputController;
        [SerializeField] Camera _MainCamera;

        #endregion

        #region Properties & Events
        public Camera MainCamera => _MainCamera;
        public event System.Action OnInitializeComplete;
        public static  event System.Action OnGamePaused;
        public bool IsGamePlaying => !IsGameFinished && IsGameBagun && !IsGamePaused;
        public ScoreManager ScoreManager { get; private set; } = null;

        #endregion

        #region Safe methods setters
        private event System.Action<int> SetScore;

        #endregion

        #region Private Variables
        private bool IsGameFinished = false;
        private bool IsGameBagun = false;
        private bool IsGamePaused = false;

        #endregion

        #region Monobehaviour
        void Start()
        {
            Initialize(this);
        }
        #endregion

        #region Public Methods
        public void RestartGame()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }

        public override void Initialize(GameManager gameManager)
        {
            if (!IsInitialized)
            {
                manager = gameManager;

                {
                    Application.targetFrameRate = 62;
                    Physics2D.gravity = Vector3.zero;

                    ScoreManager = new ScoreManager();
                    ScoreManager.Initialize(this, ref SetScore);

                    _UIController.Initialize(this);
                    _MapManager.Initialize(this, false);

                    _PlayerController.Initialize(this);
                    _PlayerController.OnAsteroidDestroy += ScorePointFromAsteroid;
                    _PlayerController.OnPlayerDestroy += PlayerDie;

                    SetScore(0);

                    _InputController.Initialize(this);
                }

                IsInitialized = true;
                OnInitialize();
                OnInitializeComplete?.Invoke();
            }
        }

        #endregion

        #region Private Methods
        private void ScorePointFromAsteroid()
        {
            SetScore.Invoke(ScoreManager.Score + SCORE_FROM_DESTROYING_ASTEROID);
        }

        private void PlayerDie()
        {
            IsGamePaused = true;
            _MapManager.StopAllAsteroidForces();
            _UIController.GetPopup<GameFinishedPopup>().Show();
        }
        void StartNewGame()
        {
            SetScore(0);
            IsGameBagun = true;
            IsGameFinished = false;
            SetGamePaused(false);
        }

        private void SetGamePaused(bool enable)
        {
            if(IsGamePaused != enable)
            {
                IsGamePaused = enable;
                if (!enable)
                    OnGamePaused?.Invoke();
            }
        }

        #endregion

        protected override void OnInitialize()
        {
            StartNewGame();
        }


    }

}
