using UnityEngine;


namespace Gameplay
{
    public class Asteroid : Poolable
    {
        static float MAX_RANDOM_FORCE = 10;
        public static event System.Action OnDestroyAny;

        [SerializeField] Rigidbody2D _Rigidbody;
        public override void Activate()
        {
            base.Activate();
            if (MapManager.AsteroidsForcesEnabled)
                _Rigidbody.WakeUp();
            else
                DisableForces();

            SetRandomizeForce();
        }

        public void DisableForces()
        {
            _Rigidbody.Sleep();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.TryGetComponent(out Asteroid asteroid))
            {
                Deactivate();
                asteroid.Deactivate();
            }
        }

        public override void Deactivate()
        {
            OnDestroyAny?.Invoke();
            base.Deactivate();
        }
        void SetRandomizeForce()
        {

            _Rigidbody.AddForce(new Vector2(
                Random.Range(1, MAX_RANDOM_FORCE) * (Random.Range(0, 2) * 2 - 1),
                Random.Range(1, MAX_RANDOM_FORCE) * (Random.Range(0, 2) * 2 - 1)));
        }
    }

}
