using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CameraFollower : MonoBehaviour
    {
        const float MOVE_MULTIPLIER = 10F;

        [SerializeField] Transform _FollowTarget;
        [SerializeField] Transform _CameraObject;



        private void Update()
        {
            _CameraObject.localPosition = Vector3.Slerp(_CameraObject.localPosition, _FollowTarget.localPosition, MOVE_MULTIPLIER *Time.deltaTime);
        }
    }
}