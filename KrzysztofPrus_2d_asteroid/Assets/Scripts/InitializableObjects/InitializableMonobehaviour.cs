using UnityEngine;
public class InitializableMonobehaviour<T> : MonoBehaviour
{
    protected T manager;
    public bool IsInitialized { get; protected set; } = false;

    public virtual void Initialize(T gameManager)
    {
        manager = gameManager;
        IsInitialized = true;
        OnInitialize();
    }

    protected virtual void OnInitialize()
    {

    }
}