public class InitializableObject<T>
{
    protected T manager;
    public bool IsInitialized { get; private set; } = false;

    public void Initialize(T gameManager)
    {
        manager = gameManager;
        IsInitialized = true;
        OnInitialize();
    }

    protected virtual void OnInitialize()
    {

    }
}
